# TP – Tri par tas

L’objectif de ce TP est d’implanter les opérations de base sur les tas afin d’arriver à l’algorithme du tri par tas.

La fonction *main* est contenue dans le fichier *tests.cpp*, et la compilation est gérée par le *Makefile*.

Le fichier d’en-têtes *Tas.h* est complet. Le seul fichier à compléter est le fichier *Tas.cpp*.

Pour tester, les tableaux manipulés sont affichés comme arbres quasi-complet dans le fichier généré *arbre.svg*. L’affichage sous forme de tableau n’est pas fourni, et est l’objet de la première question.

Le fichier test fournit deux tableaux prédéfinis : le tableau test [7,12,4,8,10,2,1,1,3,9] (qui n’est ni un tas max ni un tas min) et le tas test [9,7,8,6,4,0,2,3,5,1] (qui est un tas max). Leurs représentations sous forme d’arbre sont fournies dans l'image 1.

![HTML_LOGO](/img/img.png)

À part dans la question 2, le terme tas signifie toujours tas max dans cet exercice.

## *void afficher(int n, int\* T)*

Affiche le tableau *T* de taille *n*, sous la forme suivante : « [7,12,4,8,10,2,1,1,3,9] » (entre crochet, avec des virgules pour séparer, sans espace).

## *bool estTasMax(int n, int\* T)* et *bool estTasMin(int n, int\* T)*

Testent si *T* est un tas max ou un tas min, respectivement.

## *void tableauManuel(int n, int\* T)*

Remplit le tableau *T* de taille *n* par des valeurs rentrées à la main par l’utilisateur. On utilisera *cin* pour rentrer des valeurs à la main.

## *void tableauAleatoire(int n, int\* T, int m, int M)*

Remplit le tableau *T* avec *n* entiers aléatoires compris entre *m* et *M* (inclus).
Utiliser *rand() % k* qui renvoie un entier aléatoire entre 0 et *k* − 1. La graine est initialisée dans la fonction *main* grâce à *srand(time(NULL))*.

## *void entasser(int n, int\* T, int i)*

Entasse le nœud d’indice *i* dans le tableau *T* de taille *n*.

## *void tas(int n, int\* T)*

Transforme le tableau *T* de taille *n* en un tas. Utiliser la fonction *entasser*.

## *int trier(int n, int\* T)*

Implémente l’algorithme *TriTas* du cours et renvoie un tableau qui contient les valeurs de *T* triées en ordre croissant.

## *trierSurPlace*

Implémentation de *TriTas* qui trie le tableau sur place, c’est-à-dire sans utilisation de tableau annexe.