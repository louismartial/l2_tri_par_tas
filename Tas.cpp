#include <iostream>
#include "Tas.h"

using namespace std;


// ====================
//  TAS ET TRI PAR TAS
// ====================

void afficher(int n, int* T) {
  cout<<"[";
  if (n > 0)
    cout<<T[0];
  for (int i = 1; i < n; i++)
    cout<<","<<T[i];
  cout<<"]";
}

bool estTasMax(int n, int* T) {
  bool estTasMax = true;
  int i = 0;
  while (estTasMax && i < n) {
    if (2 * i + 1 < n && T[i] < T[2 * i + 1])
      estTasMax = false;
    if (2 * i + 2 < n && T[i] < T[2 * i + 2])
      estTasMax = false;
    i++;
  }
  return estTasMax;
}

bool estTasMin(int n, int* T) {
  bool estTasMin = true;
  int i = 0;
  while (estTasMin && i < n) {
    if (2 * i + 1 < n && T[i] > T[2 * i + 1])
      estTasMin = false;
    if (2 * i + 2 < n && T[i] > T[2 * i + 2])
      estTasMin = false;
    i++;
  }
  return estTasMin;
}

void tableauManuel(int n, int* T) {
  for (int i = 0; i < n; i++) {
    cin>>T[i];
  }
}

void tableauAleatoire(int n, int* T, int m, int M) {
  srand(time(NULL));
  for (int i = 0; i < n; i++) {
    T[i] = rand() % (M - m + 1) + m;
  }
}

void entasser(int n, int* T, int i) {
  while (2 * i + 1 < n) {
    int m = i;
    int g = 2 * i + 1;
    int d = 2 * i + 2;
    if (T[g] > T[m])
      m = g;
    if (d < n && T[d] > T[m])
      m = d;
    if (m != i) {
      int temp = T[i];
      T[i] = T[m];
      T[m] = temp;
      i = m; // très important : pour pouvoir travailler sur le nouvel emplacement de i
    }
    else
      i = n;
  }
}

void tas(int n, int* T) {
  for (int i = n; i>=0; i--) {
    entasser(n, T, i);
  }
}

int* trier(int n, int* T) {
  tas(n, T);
  for (int i = n / 2 - 1; i >= 0; i--)
    entasser(i, T, 0);
  int* Ttrie = new int[n];
  for (int i = n - 1; i >= 0; i--) {
    Ttrie[i] = T[0];
    T[0] = T[i];
    entasser(i,T,0);
  }
  return Ttrie;
}

void trierSurPlace(int n, int* T) {
  tas(n, T);
  for (int i = n / 2 - 1; i >= 0; i--)
    entasser(i, T, 0);
  for(int i = n-1; i > 0; i--) {
    int temp = T[0];
    T[0] = T[i];
    T[i] = temp;

    entasser(i, T, 0);
  }
}
